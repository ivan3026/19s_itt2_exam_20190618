#!/usr/bin/env python3

import serial
import time
import yaml

conf_file = "conf.yml"

if __name__ == "__main__":
    with open(conf_file, 'r') as stream:
        serial_cfg = yaml.safe_load(stream)

    print( "Running port {}".format( serial_cfg['device'] ) )

    with serial.Serial(serial_cfg['device'], serial_cfg['baud'], timeout=1) as ser:
        try:
            for i in range(10):
                i_chr = chr( ord('0') + i)
                print( "Sending", i_chr)
                ser.write( i_chr.encode() )
                reply = ser.readline().decode()   # read a '\n' terminated line
                print("- reply: >{}<".format(reply))

                print("- sleeping")
                time.sleep(1)

            print("Done looping")

        except KeyboardInterrupt:
            print('interrupted!')
